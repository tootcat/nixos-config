#!/usr/bin/env ruby
# frozen_string_literal: true

require 'English'
require 'ansi/code'
require 'json'
require 'open3'
require 'net/http'
require 'resolv'
require 'slop'
require 'tempfile'
require 'uri'

DEFAULT_COLMENA_NODE = 'staging-tootcat'
DEFAULT_ENV_NAME = 'staging'
DEFAULT_HOSTNAME = 'staging.toot.cat'
DEFAULT_IMAGE_NAME_MATCH = '^%<hostname>s-[0-9]{8}T[0-9]{6}Z$'
DEFAULT_N_KEEPERS = 5
DEFAULT_REGION = 'nyc3'
# Pick resolvers in Singapore, based on the wild assumption that DNS propagation
# is more than one hop away from DigitalOcean's nyc3 region.
DEFAULT_RESOLVER_REGION = 'sg'
DEFAULT_SIZE = 's-1vcpu-1gb'

COLMENA_NODE = ENV.fetch('TOOTCAT_DROPLET_COLMENA_NODE', DEFAULT_COLMENA_NODE)
ENV_NAME = ENV.fetch('TOOTCAT_DROPLET_ENV_NAME', DEFAULT_ENV_NAME)
HOSTNAME = ENV.fetch('TOOTCAT_DROPLET_HOSTNAME', DEFAULT_HOSTNAME)
IMAGE_NAME_MATCH = ENV.fetch('TOOTCAT_DROPLET_IMAGE_NAME_MATCH', DEFAULT_IMAGE_NAME_MATCH)
N_KEEPERS = Integer(ENV.fetch('TOOTCAT_DROPLET_N_KEEPERS', DEFAULT_N_KEEPERS.to_s))
REGION = ENV.fetch('TOOTCAT_DROPLET_REGION', DEFAULT_REGION)
RESOLVER_REGION = ENV.fetch('TOOTCAT_DROPLET_RESOLVER_REGION', DEFAULT_RESOLVER_REGION)
SIZE = ENV.fetch('TOOTCAT_DROPLET_SIZE', DEFAULT_SIZE)

BUSTED_HOSTNAME = 'b.u.s.t.e.d'
DESCRIPTION = <<~'EODESCRIPTION'

  Run some things for a droplet! The first positional argument will
  be interpreted as a command, which may be one of the following:

           up - ensure the droplet with given hostname is running
                and reachable
         down - ensure the droplet with given hostname is stopped
                and not reachable (disallowed for 'toot.cat')
      cleanup - delete snapshots older than the number of most recent
                under '--n-keepers'
EODESCRIPTION
PRODUCTION_HOSTNAME = 'toot.cat'
TOOTCAT_PROJECT_NAME = 'TootCat'
TOOTCAT_SLD = 'toot.cat'

def main
  options = Slop.parse do |o|
    o.string '-H', '--hostname',
             "hostname (default #{HOSTNAME.inspect})",
             default: HOSTNAME
    o.string '-I', '--image-name-match',
             "image name match (default: #{IMAGE_NAME_MATCH.inspect})",
             default: IMAGE_NAME_MATCH
    o.string '-R', '--region',
             "region (default: #{REGION.inspect})",
             default: REGION
    o.string '-S', '--size',
             "size (default: #{SIZE.inspect})",
             default: SIZE
    o.string '-K', '--ssh-keys',
             'ssh keys (default pulled from keys.nix)'
    o.string '-e', '--env-name',
             "environment name to use in droplet tag (default: #{ENV_NAME.inspect})",
             default: ENV_NAME
    o.bool '-6', '--with-ipv6',
           'make sure all the ipv6 bits are turned on (default: false)'
    o.string '--resolver-region',
             "resolver region for DNS lookups (default: #{RESOLVER_REGION.inspect})",
             default: RESOLVER_REGION
    o.int '--n-keepers',
          "number of snapshots to keep at cleanup time (default: #{N_KEEPERS})",
          default: N_KEEPERS
    o.bool '--force-cleanup',
           'clean up snapshots without asking (default: false)'
    o.on '-h', '--help', 'show this text and exit' do
      puts o
      puts DESCRIPTION
      return 0
    end
  end

  Droplet.new(**options).run!(ARGV.first || 'help')

  0
rescue Interrupt
  86
end

class Droplet
  def initialize(
    env_name: DEFAULT_ENV_NAME,
    force_cleanup: false,
    hostname: DEFAULT_HOSTNAME,
    image_name_match: DEFAULT_IMAGE_NAME_MATCH,
    n_keepers: DEFAULT_N_KEEPERS,
    region: DEFAULT_REGION,
    resolver_region: DEFAULT_RESOLVER_REGION,
    size: DEFAULT_SIZE,
    ssh_keys: '',
    with_ipv6: false
  )
    ssh_keys = (ssh_keys || '').strip.split

    @env_name = env_name
    @force_cleanup = force_cleanup
    @hostname = hostname
    @image_name_match = image_name_match
    @n_keepers = n_keepers
    @region = region
    @resolver_region = resolver_region
    @size = size
    @ssh_keys = ssh_keys.empty? ? load_default_ssh_keys : ssh_keys
    @with_ipv6 = with_ipv6
  end

  def run!(cmd = 'up')
    prep_env

    start_time = Time.now

    send("run_#{cmd}")

    return if cmd == 'help'

    log_ok "#{cmd} complete (#{format('%.2f', Time.now - start_time)}s)"
  end

  private

  attr_reader :env_name, :hostname, :image_name_match, :n_keepers, :region, \
              :resolver_region, :ssh_keys, :size

  def with_ipv6?
    @with_ipv6
  end

  def force_cleanup?
    @force_cleanup
  end

  def prep_env
    Dir.chdir(sh!(['git', 'rev-parse', '--show-toplevel']).strip)
    ENV['DIGITALOCEAN_ACCESS_TOKEN'] ||= File.read(
      'secrets/mastodon/digitalocean-key'
    ).strip
  end

  def run_help
    log_warn '(try running with --help)'
  end

  def run_up
    ensure!
    retry_with_backoff! { ping! }
  end

  def run_down
    raise 'one must not delete production' if hostname == PRODUCTION_HOSTNAME

    droplet = find_cfg
    if droplet.nil?
      log_warn 'droplet unavailable; cannot take snapshot'
    else
      begin
        doctl!(
          ['compute', 'droplet-action', 'shutdown', '--wait', droplet.fetch('id').to_s],
          json: false
        )
      rescue StandardError
        nil
      end
      doctl!(
        [
          'compute', 'droplet-action', 'snapshot',
          '--wait',
          '--snapshot-name', "#{hostname}-#{Time.now.utc.strftime('%Y%m%dT%H%M%SZ')}",
          droplet.fetch('id').to_s,
          '-o', 'json'
        ]
      )

      begin
        doctl!(['compute', 'droplet', 'delete', hostname, '--force'], json: false)
      rescue StandardError => e
        log_err "failed to delete droplet #{hostname}: #{e}"
        log_err 'already deleted?'
      end
    end

    ensure_dns_records!(nullify: true)
  end

  def run_cleanup
    raise 'one must keep an snapshot' if n_keepers <= 0

    all_images = list_valid_images.select { |entry| %w[snapshot custom].include?(entry.fetch('type')) }
    stale = all_images - all_images[([-1 * all_images.length, -1 * n_keepers].max)..]

    log_info "stale snapshot count: #{stale.length}"

    stale.each do |entry|
      name = entry.fetch('name')
      log_warn "delete stale snapshot #{name.inspect}?"
      print '[Yn] '

      if !force_cleanup? && $stdin.gets.strip == 'n'
        log_warn "skipping delete of snapshot #{name.inspect}"
        next
      end

      doctl!(
        ['compute', 'image', 'delete', entry.fetch('id').to_s, '--force'],
        json: false
      )
    end
  end

  def ensure!
    ensure_droplet!
    ensure_dns_records!
  end

  def ensure_droplet!
    image = find_image
    raise "no image found matching #{image_name_match}" if image.nil?

    project = find_project
    raise "no project found named #{TOOTCAT_PROJECT_NAME}" if project.nil?

    droplet = find_cfg
    if droplet.nil?
      do_ssh_keys = ssh_keys.map { |pubkey| ssh_key_fingerprint(pubkey) }

      droplet = doctl!([
        'compute', 'droplet', 'create',
        '--image', image.fetch('id').to_s,
        '--size', size,
        '--region', region,
        '--tag-name', "env:#{env_name}",
        '--ssh-keys', do_ssh_keys.map(&:strip).join(','),
        with_ipv6? ? '--enable-ipv6' : nil,
        '--wait',
        hostname,
        '-o', 'json'
      ].compact).fetch(0)
    end

    droplet_id = droplet.fetch('id')
    project_id = project.fetch('id')

    doctl!([
             'projects', 'resources', 'assign',
             project_id,
             '--resource', "do:droplet:#{droplet_id}",
             '-o', 'json'
           ])
  end

  def ensure_dns_records!(nullify: false)
    ipv4_addr = nullify ? '127.0.0.1' : find_ip('v4')
    raise 'failed to get ipv4 address' if ipv4_addr.nil? || ipv4_addr == BUSTED_HOSTNAME

    ipv6_addr = BUSTED_HOSTNAME

    if with_ipv6?
      ipv6_addr = nullify ? '0:0:0:0:0:0:0:1' : find_ip('v6')
      raise 'failed to get ipv6 address' if ipv6_addr.nil? || ipv6_addr == BUSTED_HOSTNAME
    end

    domain = find_domain
    raise "no domain found named #{TOOTCAT_SLD}" if domain.nil?

    subdomain = hostname.sub(/\.#{TOOTCAT_SLD}$/, '')
    grafana_subdomain = "grafana.#{subdomain}"

    [
      { type: 'A', addr: ipv4_addr },
      with_ipv6? ? { type: 'AAAA', addr: ipv6_addr } : nil
    ].compact.each do |rec|
      [subdomain, grafana_subdomain].each do |name|
        dns_record = find_dns_record(name)

        rec_args = [
          '--record-type', rec.fetch(:type),
          '--record-data', rec.fetch(:addr),
          '--record-name', name,
          '--record-ttl', '60'
        ]

        if dns_record.nil?
          dns_record = doctl!([
            'compute', 'domain', 'records', 'create', TOOTCAT_SLD,
            rec_args,
            '-o', 'json'
          ].flatten).fetch(0)
        end

        doctl!([
          'compute', 'domain', 'records', 'update', TOOTCAT_SLD,
          '--record-id', dns_record.fetch('id').to_s,
          rec_args,
          '-o', 'json'
        ].flatten)

        next if rec.fetch(:type) != 'A'

        retry_with_backoff!(max_interval_seconds: 30, max_wait_seconds: 300) do
          check_dns_propagation!("#{name}.#{TOOTCAT_SLD}", rec.fetch(:addr))
        end
      end
    end
  end

  def find_cfg
    doctl!(['compute', 'droplet', 'list', '-o', 'json']).find { |e| e.fetch('name') == hostname }
  rescue StandardError
    nil
  end

  def find_image
    list_valid_images.fetch(-1)
  rescue StandardError
    nil
  end

  def list_valid_images
    doctl!(['compute', 'image', 'list-user', '-o', 'json']).select do |entry|
      valid_image?(entry)
    end.sort do |a, b|
      a.fetch('name') <=> b.fetch('name')
    end
  end

  def valid_image?(entry)
    entry.fetch('regions', []).include?(region) &&
      entry.fetch('status', '') == 'available' &&
      entry.fetch('name', '') =~ /#{format(image_name_match, hostname: hostname)}/
  end

  def find_project
    doctl!(['projects', 'list', '-o', 'json']).find { |e| e.fetch('name') == TOOTCAT_PROJECT_NAME }
  rescue StandardError
    nil
  end

  def find_domain
    doctl!(['compute', 'domain', 'list', '-o', 'json']).find { |e| e.fetch('name') == TOOTCAT_SLD }
  rescue StandardError
    nil
  end

  def find_dns_record(name)
    doctl!([
             'compute', 'domain', 'records', 'list', TOOTCAT_SLD, '-o', 'json'
           ]).find { |e| e.fetch('name') == name }
  rescue StandardError
    nil
  end

  def find_ip(v = 'v6')
    addr = (find_cfg || {})
           .fetch('networks', {})
           .fetch(v, [])
           .find { |net| net.fetch('type') == 'public' }

    (addr || {}).fetch('ip_address', BUSTED_HOSTNAME)
  end

  def ping!
    ipv4 = find_ip('v4')

    raise 'missing droplet configuration' if ipv4 == BUSTED_HOSTNAME

    if sh!([
             'ssh',
             '-o', 'ConnectTimeout=3',
             '-o', 'UserKnownHostsFile=/dev/null',
             '-o', 'StrictHostKeyChecking=off',
             "root@#{ipv4}",
             'whoami'
           ]).strip == 'root'
      log_ok '✨🍕✨ connected successfully'
      return
    end

    raise "failed to ping #{hostname} (root@#{ipv4})"
  end

  def check_dns_propagation!(name, addr)
    log_warn "#{name.inspect} getaddress (via region #{resolver_region.inspect})"
    actual_addr = Resolv::DNS.new(nameserver: resolver_ips(resolver_region)).getaddress(name)

    if actual_addr.to_s == addr
      log_ok "#{name.inspect} resolving to #{actual_addr} (via region #{resolver_region.inspect})"
      return
    end

    raise "address not propagated yet for #{name.inspect} " \
          + "(#{actual_addr} != #{addr})"
  end
end

def resolver_ips(region_code, count: 5)
  req_url = URI.parse("https://public-dns.info/nameserver/#{region_code}.json")
  response = Net::HTTP.get_response(req_url)

  raise "unable to get DNS resolvers: code=#{response.code}" if response.code.to_i != 200

  JSON.parse(response.body)
      .select { |entry| entry['reliability'] >= 0.99999 }
      .map { |entry| entry['ip'] }
      .sample(count)
end

def load_default_ssh_keys
  out, err, status = Open3.capture3(
    'nix-instantiate', '--eval', '--json', '-',
    stdin_data: '(import ./keys.nix {}).users.users.root.openssh.authorizedKeys.keys'
  )

  unless status.success?
    log_err "ERROR (stderr): #{err.chomp}"
    log_err "ERROR (stdout): #{out.chomp}"
    raise "command failed: #{cmd}"
  end

  JSON.parse(out)
end

def ssh_key_fingerprint(pub_key)
  Tempfile.create('pubkey') do |f|
    f.puts pub_key
    f.close

    md5_sig = sh!(['ssh-keygen', '-l', '-E', 'md5', '-f', f.path])
    return md5_sig.split.fetch(1).gsub(/MD5:/, '').strip
  end
end

def doctl!(cmd, json: true)
  result = sh!(['doctl'].concat(cmd))
  result = JSON.parse(result) if json
  result
end

def sh!(cmd)
  log_info cmd.map(&:inspect).join(' ').to_s

  out, err, status = Open3.capture3(*cmd)

  unless status.success?
    log_err "ERROR (stderr): #{err.chomp}"
    log_err "ERROR (stdout): #{out.chomp}"
    raise "command failed: #{cmd}"
  end

  out
end

def retry_with_backoff!(max_interval_seconds: 15, max_wait_seconds: 120)
  start_time = Time.now
  n = 0

  loop do
    raise 'oh no timeout' if (Time.now - start_time) > max_wait_seconds

    sleep_seconds = Integer(
      [0, [1 * 1.75**n + rand, max_interval_seconds].min].max
    )

    begin
      yield
      return
    rescue StandardError => e
      log_err "failed with: #{e}; waiting #{sleep_seconds}"
    end

    sleep sleep_seconds
    n += 1
  end
end

def log_info(txt)
  log(:white, '>', txt)
end

def log_warn(txt)
  log(:yellow, '!', txt)
end

def log_err(txt)
  log(:red, 'x', txt)
end

def log_ok(txt)
  log(:green, '*', txt)
end

def log(color, prefix, txt)
  warn(ANSI.ansi(color.to_sym) { "#{prefix} #{txt}" })
end

exit main if __FILE__ == $PROGRAM_NAME
