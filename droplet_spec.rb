#!/usr/bin/env ruby
# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'droplet'

describe Droplet do
  before do
    @droplet = Droplet.new
  end

  it 'knows how to run' do
    _(@droplet).must_respond_to :run!
  end

  it 'has a hostname' do
    _(@droplet.send(:hostname)).must_equal DEFAULT_HOSTNAME
  end

  it 'has a region' do
    _(@droplet.send(:region)).must_equal DEFAULT_REGION
  end

  it 'has an image name match' do
    _(@droplet.send(:image_name_match)).must_equal DEFAULT_IMAGE_NAME_MATCH
  end

  it 'has a size' do
    _(@droplet.send(:size)).must_equal DEFAULT_SIZE
  end
end
