{ pkgs }:

let
  gems = pkgs.bundlerEnv {
    name = "tootcat-gems";
    gemdir = ./.;
  };
in pkgs.mkShell {
  name = "tootcat-shell";
  buildInputs = [
    gems
    gems.wrappedRuby

    pkgs.bundix
    pkgs.colmena
    pkgs.coreutils
    pkgs.diffutils
    pkgs.doctl
    pkgs.git-crypt
    pkgs.gnused
    pkgs.jq
    pkgs.prefetch-yarn-deps
    pkgs.nix-prefetch-git
    pkgs.nixfmt
    pkgs.yarn-lock-converter
  ];
}
