{ pkgs, lib, config, ... }:
with lib;
let cfg = config.tootcat.metrics_collector;
in {
  options.tootcat.metrics_collector = {
    grafanaDomain = mkOption {
      type = types.str;
      description = "What domain grafana will be served ad";
    };
    grafanaPort = mkOption {
      type = types.port;
      description = "Internal http port grafana listens on";
      default = 5551;
    };
    prometheusPort = mkOption {
      type = types.port;
      description = "Internal http port prometheus listens on";
      default = 9001;
    };
    mastodonStatsExporter = mkOption {
      type = types.str;
      description = "Address of the mastodon statsd exporter";
    };
    nodeExporter = mkOption {
      type = types.str;
      description = "Address of the node exporter";
    };
    configNginx = mkEnableOption
      (lib.mdDoc "Whether to configure a nginx vhost to serve grafana");
  };
  config.services.grafana = {
    enable = true;
    dataDir = "/var/lib/grafana";
    settings = {
      server = {
        domain = "${cfg.grafanaDomain}";
        root_url = "https://${cfg.grafanaDomain}/";
        http_port = cfg.grafanaPort;
        protocol = "http";
      };
      users.allow_sign_up = false;
    };
  };

  config.services.prometheus = {
    enable = true;
    port = cfg.prometheusPort;

    scrapeConfigs = [
      {
        job_name = "mastodon";
        metrics_path = "/metrics";
        honor_labels = true;
        scrape_interval = "30s";
        scheme = "http";
        params = { format = [ "prometheus" ]; };
        static_configs = [{ targets = [ cfg.mastodonStatsExporter ]; }];
      }
      {
        job_name = "node";
        metrics_path = "/metrics";
        honor_labels = true;
        scrape_interval = "30s";
        scheme = "http";
        params = { format = [ "prometheus" ]; };
        static_configs = [{ targets = [ cfg.nodeExporter ]; }];
      }
    ];
  };

  config.services.nginx = lib.mkIf cfg.configNginx {
    virtualHosts."${cfg.grafanaDomain}" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString cfg.grafanaPort}";
        proxyWebsockets = true; # needed if you need to use WebSocket
        extraConfig =
          # required when the target is also TLS server with multiple hosts
          "proxy_ssl_server_name on;" +
          # required when the server wants to use HTTP Authentication
          "proxy_pass_header Authorization;";
      };
    };
  };

}
