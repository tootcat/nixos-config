{
  inputs = {
    nixpkgs = { url = "github:NixOS/nixpkgs/nixpkgs-unstable"; };

    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    eisfunke-config-nixos = {
      url = "git+https://git.eisfunke.com/config/nixos.git";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ nixpkgs, nixos-generators, ... }: {
    colmena = {
      meta = {
        nixpkgs = import nixpkgs { system = "x86_64-linux"; };
        specialArgs = { inherit (inputs) eisfunke-config-nixos; };
      };

      tootcat = { ... }: {
        deployment.targetHost = "toot.cat";

        imports = [
          ./configuration.nix
          ./keys.nix
          ./hardware-configuration.nix
          ./networking.nix # generated at runtime by nixos-infect
          ./grafana.nix
          ./stats_exporters.nix
          ./mastodon.nix
        ];
      };

      tootcat-staging = { ... }: {
        deployment.targetHost = "staging.toot.cat";

        imports = [
          ./configuration.nix
          ./keys.nix
          ./grafana.nix
          ./stats_exporters.nix
          nixos-generators.nixosModules.do
          ./mastodon-staginginstance.nix
        ];
      };
    };

    devShells."x86_64-linux".default =
      import ./shell.nix { pkgs = nixpkgs.legacyPackages."x86_64-linux"; };

    # A base image for building nixos droplets from, that has the ssh authorized_keys already embedded.
    # to build an image (which will be at ./result/):
    # nix build .#dropletBase
    dropletBase = nixos-generators.nixosGenerate {
      system = "x86_64-linux";
      modules = [ ./configuration.nix ./keys.nix ];
      format = "do";
    };
  };
}
