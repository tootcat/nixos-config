{ pkgs, eisfunke-config-nixos, ... }:

let
  srcOverride = (pkgs.callPackage ./mastodon/source.nix { });
  mastodon = (eisfunke-config-nixos.packages.x86_64-linux.glitch-soc.override {
    pname = "mastodon";
    srcOverride = srcOverride.src;
    gemset = ./mastodon/gemset.nix;
    version = srcOverride.version;
    yarnHash = srcOverride.yarnHash;
  });
in {
  services = {
    mastodon = {
      streamingProcesses = 2;
      enable = true;
      package = mastodon;
      configureNginx = true;
      localDomain = "staging.toot.cat";

      smtp.fromAddress = "server2022+staging@toot.cat";

      extraConfig = {
        # NOTE: strictly allow-list only given that the staging instance is not always running
        LIMITED_FEDERATION_MODE = "true";

        AUTHORIZED_FETCH = "true";
        MAX_TOOT_CHARS = "1000000";
        STATSD_ADDR = "localhost:9125";

        AWS_ACCESS_KEY_ID = "stagingtootcat";
        EXTRA_DATA_HOSTS = "https://blob.jortage.com";
        S3_ALIAS_HOST = "pool.jortage.com/stagingtootcat";
        S3_BUCKET = "stagingtootcat";
        S3_ENABLED = "true";
        S3_ENDPOINT = "https://pool-api.jortage.com";
        S3_HOSTNAME = "pool-api.jortage.com";
        S3_PROTOCOL = "https";
        S3_REGION = "jort";
        S3_SIGNATURE_VERSION = "v4";
      };

      extraEnvFiles = [ "/var/lib/mastodon/secrets/extra-secrets-staging.env" ];

      elasticsearch.host = "127.0.0.1";

      sidekiqProcesses = {
        default = { # impacts local user timeline responsiveness
          jobClasses = [ "default" ];
          threads = 10;
        };
        federation = {
          jobClasses = [ "ingress" "push" "pull" ];
          threads = 10;
        };
        mailers = {
          jobClasses = [ "mailers" ];
          threads = 5;
        };
        scheduler = {
          jobClasses = [ "scheduler" ];
          threads = 5;
        };
      };
    };

    nginx = {
      # Set the worker connection limit to roughly 2x the current active users
      # and the number of open files to roughly 10 files per that number of
      # users. This should give us ample headroom for normal operations and
      # still offer some protection against some forms of DDoS attack.

      appendConfig = ''
        worker_processes 6;
        worker_rlimit_nofile 20000;
      '';

      eventsConfig = ''
        worker_connections 2000;
      '';

      virtualHosts."staging.toot.cat".locations."/".extraConfig = ''
        if ($http_user_agent ~ Content-Nation) {
          return 404;
        }
      '';
    };

    postgresqlBackup = {
      enable = false;
      databases = [ "mastodon" ];
      # run at 12:15AM in US/Pacific
      startAt = "*-*-* 08:15:00";
    };

    postgresql = {
      settings = {
        max_connections = 150;
        max_replication_slots = 10;
        max_wal_senders = 10;
        shared_buffers = "100MB";
        wal_level = "logical";
      };
    };

    elasticsearch = {
      enable = true;
      package = pkgs.elasticsearch7;
      extraConf = ''
        ingest.geoip.downloader.enabled: false
        xpack.security.enabled: false
      '';
    };

    fail2ban = {
      enable = true;
      bantime-increment = {
        enable = true;
        overalljails = true;
      };
    };
  };

  nixpkgs.config.allowUnfree = true; # required because of elasticsearch license

  deployment.keys = let
    mastodonSecret = name: {
      inherit name;
      value = {
        keyFile = ./secrets/mastodon/${name};
        destDir = "/var/lib/mastodon/secrets";
        user = "mastodon";
        group = "mastodon";
      };
    };
  in builtins.listToAttrs
  (builtins.map mastodonSecret [ "extra-secrets-staging.env" ]);

  networking.firewall.allowedTCPPorts = [ 80 443 ];
  networking.enableIPv6 = false;
  networking.hostName = "staging-tootcat";

  # 111MB makes 111% of services.postgresql.settings.shared_buffers
  boot.kernel.sysctl."kernel.shmmax" = 111 * 1000 * 1000;

  # wanted by redis with jemalloc
  boot.kernel.sysctl."vm.overcommit_memory" = 1;

  security.acme = {
    acceptTerms = true;
    defaults.email = "dev@vvn.space";
  };

  tootcat = let
    nodeExporterPort = 19991;
    mastodonStatsExporterPort = 19992;
  in {
    metrics_collector = {
      grafanaPort = 5551;
      grafanaDomain = "grafana.staging.toot.cat";
      mastodonStatsExporter = "127.0.0.1:${toString mastodonStatsExporterPort}";
      nodeExporter = "127.0.0.1:${toString nodeExporterPort}";
      configNginx = true;
    };
    metrics_exporter = {
      inherit nodeExporterPort;
      inherit mastodonStatsExporterPort;
    };
  };
}
