{ lib, ... }: {
  # This file was populated at runtime with the networking
  # details gathered from the active system.
  networking = {
    nameservers = [ "8.8.8.8" ];
    defaultGateway = "143.244.160.1";
    defaultGateway6 = "2604:a880:400:d0::1";
    dhcpcd.enable = false;
    usePredictableInterfaceNames = lib.mkForce false;
    interfaces = {
      eth0 = {
        ipv4.addresses = [
          {
            address = "143.244.160.92";
            prefixLength = 20;
          }
          {
            address = "10.10.0.7";
            prefixLength = 16;
          }
        ];
        ipv6.addresses = [
          {
            address = "2604:a880:400:d0::2354:2001";
            prefixLength = 64;
          }
          {
            address = "fe80::a400:6aff:fecc:f21e";
            prefixLength = 64;
          }
        ];
        ipv4.routes = [{
          address = "143.244.160.1";
          prefixLength = 32;
        }];
        ipv6.routes = [{
          address = "2604:a880:400:d0::1";
          prefixLength = 128;
        }];
      };

    };
  };
  services.udev.extraRules = ''
    ATTR{address}=="a6:00:6a:cc:f2:1e", NAME="eth0"
    ATTR{address}=="62:33:80:be:53:d2", NAME="eth1"
  '';
}
