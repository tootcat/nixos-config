{ pkgs, eisfunke-config-nixos, ... }:

let
  srcOverride = (pkgs.callPackage ./mastodon/source.nix { });
  mastodon = (eisfunke-config-nixos.packages.x86_64-linux.glitch-soc.override {
    pname = "mastodon";
    srcOverride = srcOverride.src;
    gemset = ./mastodon/gemset.nix;
    version = srcOverride.version;
    yarnHash = srcOverride.yarnHash;
  });
in {
  services = {
    mastodon = {
      streamingProcesses = 2;
      enable = true;
      package = mastodon;
      configureNginx = true;
      localDomain = "toot.cat";

      smtp.fromAddress = "server2022@toot.cat";

      extraConfig = {
        AUTHORIZED_FETCH = "true";
        MAX_TOOT_CHARS = "1000000";
        STATSD_ADDR = "localhost:9125";

        AWS_ACCESS_KEY_ID = "tootcat";
        EXTRA_DATA_HOSTS = "https://blob.jortage.com";
        S3_ALIAS_HOST = "pool.jortage.com/tootcat";
        S3_BUCKET = "tootcat";
        S3_ENABLED = "true";
        S3_ENDPOINT = "https://pool-api.jortage.com";
        S3_HOSTNAME = "pool-api.jortage.com";
        S3_PROTOCOL = "https";
        S3_REGION = "jort";
        S3_SIGNATURE_VERSION = "v4";
      };

      extraEnvFiles =
        [ "/var/lib/mastodon/secrets/extra-secrets-production.env" ];

      elasticsearch.host = "127.0.0.1";

      sidekiqProcesses = {
        default = { # impacts local user timeline responsiveness
          jobClasses = [ "default" ];
          threads = 20;
        };
        federation = {
          jobClasses = [ "ingress" "push" "pull" ];
          threads = 20;
        };
        mailers = {
          jobClasses = [ "mailers" ];
          threads = 10;
        };
        scheduler = {
          jobClasses = [ "scheduler" ];
          threads = 10;
        };
      };
    };

    nginx = {
      # Set the worker connection limit to roughly 2x the current active users
      # and the number of open files to roughly 10 files per that number of
      # users. This should give us ample headroom for normal operations and
      # still offer some protection against some forms of DDoS attack.

      appendConfig = ''
        worker_processes 6;
        worker_rlimit_nofile 20000;
      '';

      eventsConfig = ''
        worker_connections 2000;
      '';

      virtualHosts."toot.cat".locations."/".extraConfig = ''
        if ($http_user_agent ~ Content-Nation) {
          return 404;
        }
      '';
    };

    postgresqlBackup = {
      enable = true;
      databases = [ "mastodon" ];
      # run at 12:15AM in US/Pacific
      startAt = "*-*-* 08:15:00";
    };

    postgresql = {
      settings = {
        max_connections = 300;
        max_replication_slots = 10;
        max_wal_senders = 10;
        shared_buffers = "200MB";
        wal_level = "logical";
      };
    };

    elasticsearch = {
      enable = true;
      package = pkgs.elasticsearch7;
      extraConf = ''
        ingest.geoip.downloader.enabled: false
        xpack.security.enabled: false
      '';
    };

    fail2ban = {
      enable = true;
      bantime-increment = {
        enable = true;
        overalljails = true;
      };
    };
  };

  nixpkgs.config.allowUnfree = true; # required because of elasticsearch license

  deployment.keys = let
    mastodonSecret = name: {
      inherit name;
      value = {
        keyFile = ./secrets/mastodon/${name};
        destDir = "/var/lib/mastodon/secrets";
        user = "mastodon";
        group = "mastodon";
      };
    };
  in builtins.listToAttrs (builtins.map mastodonSecret [
    "otp-secret"
    "secret-key-base"
    "vapid-private-key"
    "vapid-public-key"
    "aws-secret-access-key"
    "extra-secrets-production.env"
  ]);

  networking.firewall.allowedTCPPorts = [ 80 443 ];
  networking.hostName = "production-tootcat";

  # 222MB makes 111% of services.postgresql.settings.shared_buffers
  boot.kernel.sysctl."kernel.shmmax" = 222 * 1000 * 1000;

  # wanted by redis with jemalloc
  boot.kernel.sysctl."vm.overcommit_memory" = 1;

  security.acme = {
    acceptTerms = true;
    defaults.email = "tc.certbot.2022@wooz.dev";
  };

  tootcat = let
    nodeExporterPort = 19991;
    mastodonStatsExporterPort = 19992;
  in {
    metrics_collector = {
      grafanaPort = 5551;
      grafanaDomain = "grafana.ops.toot.cat";
      mastodonStatsExporter = "127.0.0.1:${toString mastodonStatsExporterPort}";
      nodeExporter = "127.0.0.1:${toString nodeExporterPort}";
      configNginx = true;
    };
    metrics_exporter = {
      inherit nodeExporterPort;
      inherit mastodonStatsExporterPort;
    };
  };
}
