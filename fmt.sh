#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

# Run a check first since forcibly formatting everything results in timestamp
# changes that cachebust the flake shell integration which prints a bunch.
if ! nixfmt -c $(git ls-files '*.nix'); then
  nixfmt $(git ls-files '*.nix')
fi
