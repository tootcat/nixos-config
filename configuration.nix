{ pkgs, ... }: {
  imports = [ ];

  boot.tmp.cleanOnBoot = true;

  zramSwap.enable = true;

  services.postgresql.package = pkgs.postgresql_13;

  services.openssh.enable = true;

  environment.systemPackages = with pkgs; [ tmux neovim ];

  system.stateVersion = "22.05";
}
