# 2022 scripted upgrade on Nix - notes

The general flow for small site changes:
* make changes in the glitch fork git repo
* commit them, push them
* run update.sh
* colmena apply

if you want to change anything on the server, nixos-config has to change. If the change involves the mastodon code, update.sh needs to run (and will change nixos-config to point at newer mastodon code)
and colmena apply is your "build the server's entire config locally and then pave over the running server with it" command

## Viv explains

for update.sh you first have to understand what the files in https://gitlab.com/tootcat/nixos-config/-/tree/main/mastodon mean

they point to a specific commit of the tootcat glitch-soc fork

update.sh's job is to download glitch-soc from a url, look at it, and update the four files mentioned above so that they now point to *that* glitch-soc

the syntax isn't really important. it's about the data.
* https://gitlab.com/tootcat/nixos-config/-/blob/main/mastodon/source.nix
  * this is a git url, commit hash, checksum.
* https://gitlab.com/tootcat/nixos-config/-/blob/main/mastodon/version.nix
  * this is just the version string
* https://gitlab.com/tootcat/nixos-config/-/blob/main/mastodon/yarn-sha256.nix
  * this is a hash of all the yarn dependencies of glitch-soc, so those are frontend javascript dependencies
* https://gitlab.com/tootcat/nixos-config/-/blob/main/mastodon/gemset.nix
  * these are all the ruby gems that glitch-soc depends on

Note that when update.sh runs update.sh inside of itself, it's not running itself but a different update.sh: https://github.com/vivlim/nixpkgs/blob/99416f6c2c9401b9e555b81be6845f3177b672a6/pkgs/servers/mastodon/update.sh

when you run colmena build, nix fetches and builds the entire set of packages and system config that will be deployed to the server. this includes the mastodon package, and part of the tootcat nixos config says that instead of fetching the vanilla mastodon package, it should instead grab and build the source that the four files linked above point at.

## Whack the Side of the TV

* `tootctl clear cache`
* Try:
  * `@RAILS_ENV=production bundle exec rails assets:clobber`
  * `assets:precompile`
* check https://toot.cat/health