# 2022 scripted upgrade on Nix

* [ash's instructions](https://gitlab.com/tootcat/nixos-config)
* [2022 scripted upgrade on Nix - notes](../../nixos-config/docs/deployment%20notes.md)

This entire process is executed from your local machine, i.e. not on the server. You should be in the nixos-config folder (not anywhere else and not in a subfolder of that). You will need Nix and of course Git.

Packages:
* nix-bin
* git (I think)

## Setup: Installing Nix

* Don't use `apt`.
* Use the instructions here: https://nixos.org/download.html
  * ...which basically amount to `sh <(curl -L https://nixos.org/nix/install) --daemon`, I think, but maybe check that. 2022-11-09

## Setup: fetching the GlitchSoc repo

Do this *outside* the nixos-config folder -- in the root folder of wherever you're keeping all your local git repos generally seems like a good place to cd to first:

 `git clone git@gitlab.com:tootcat/glitch.git`

...then cd into it:

 `cd glitch`

Add the official GS repo as a remote called "upstream":

`git remote add upstream https://github.com/glitch-soc/mastodon.git`

Not sure why this is needed, but in any case we're fetching the latest from "upstream":

`git fetch upstream`

Wave an appropriate rubber chicken:

`git rebase upstream/main`

## Setup: Unlocking

W note: Authorized ssh keys are in https://gitlab.com/tootcat/nixos-config/-/blob/main/configuration.nix#L16-21
* ...otherwise known as {project folder}/configuration.nix
* They're presumably encrypted in the public repo, though I don't yet understand the details.

To check if you've unlocked the repo:
 `git config --local --get filter.git-crypt.smudge`
On my systems, this outputs 
 `"git-crypt" smudge`
if unlocked, and otherwise nothing.

I keep the secret key on my desktop in {TootCat project folder}`/security/tootcat-key`. So, from inside the nixos-config folder, the command to use is:
* **`git-crypt unlock ../security/tootcat-key`**

## Update prep
 
On the local machine -- prepping the update:
* **`nix develop`** - to enter the shell environment (has colmena in it); this can take a few seconds
* **`colmena apply`** - this takes on the order of minutes

If feeling extra-paranoid, `colmena build` goes through the motions without attempting to deploy.

**Backup** (there is a nightly automated backup, but it's best to do another one right before possible failure):
* **`ssh [user]@toot.cat`** (I use root. Don't use root. Bad Woozle.)
* **`systemctl start postgresqlBackup-mastodon.service`**

## Actual update

On the local machine -- actually doing the update:
* **`./update.sh`**

Possible errors:
* "error: experimental Nix feature 'nix-command' is disabled; use '--extra-experimental-features nix-command' to override"
  * add "`experimental-features = nix-command flakes`" to `\etc\nix.conf`

