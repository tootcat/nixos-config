{ fetchpatch, ... }:

{
  patches = [
    (fetchpatch {
      name = "eisfunke-yarn-typescript.patch";
      url =
        "https://git.eisfunke.com/config/nixos/-/raw/2bfd28ad0d213b98b77ca330ece0bed5e1147e1b/packages/mastodon/yarn-typescript.patch";
      hash = "sha256-+WvaZGtdrdCl59rn0zZvO62Y3XIxhTgJYoyHB+45LXw=";
    })
    (fetchpatch {
      name = "tootcat-patches-20240217T131437Z.patch";
      url = "https://gitlab.com/tootcat/glitch/-/merge_requests/25.patch";
      hash = "sha256-qyskWtv6nW8FhtbGEyonhbzAZ/a35vLIp8NuaIYBfZU=";
    })
  ];
}
