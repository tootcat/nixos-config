{ pkgs, ... }:

let
  srcOverride = pkgs.callPackage ./mastodon/source.nix { };
  mastodon = pkgs.mastodon.override {
    version = srcOverride.version;
    srcOverride = srcOverride.src;
    gemset = ./mastodon/gemset.nix;
    yarnHash = srcOverride.yarnHash;
  };
in {
  services = {
    mastodon = {
      enable = true;
      package = mastodon;
      configureNginx = true;
      localDomain = "tootdev.s.vvn.space";

      smtp.fromAddress = "server2022@toot.cat";

      extraConfig = {
        STATSD_ADDR = "localhost:9125";
        MAX_TOOT_CHARS = "1000000";
      };

      sidekiqProcesses = {
        all = {
          jobClasses = [ ];
          threads = 25;
        };
        default = { # impacts local user timeline responsiveness
          jobClasses = [ "default" ];
          threads = 20;
        };
        federation = {
          jobClasses = [ "ingress" "push" "pull" ];
          threads = 20;
        };
      };
    };

    postgresqlBackup = {
      enable = false;
      databases = [ "mastodon" ];
    };

  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];
  networking.hostName = "test-tootcat";

  security.acme = {
    acceptTerms = true;
    defaults.email = "dev@vvn.space";
    defaults.server =
      "https://acme-staging-v02.api.letsencrypt.org/directory"; # letsencrypt staging environment, so we don't hit rate limits
  };

  tootcat = let
    nodeExporterPort = 19991;
    mastodonStatsExporterPort = 19992;
  in {
    metrics_collector = {
      grafanaPort = 5551;
      grafanaDomain = "grafana.tootdev.s.vvn.space";
      mastodonStatsExporter = "127.0.0.1:${toString mastodonStatsExporterPort}";
      nodeExporter = "127.0.0.1:${toString nodeExporterPort}";
      configNginx = true;
    };
    metrics_exporter = {
      inherit nodeExporterPort;
      inherit mastodonStatsExporterPort;
    };
  };
}
